package pandicornios.ucbcba.api_connectivity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val restApiAdapter = RestApiAdapter()
        val endPoint = restApiAdapter.connexionApi()
        val bookResponseCall = endPoint.getAllPost()

        button.setOnClickListener{
            System.out.println("Entro al boton");
            bookResponseCall.enqueue( object : Callback<List<Post>> {
                override fun onFailure(call: Call<List<Post>>, t: Throwable) {
                    t?.printStackTrace()
                }

                override fun onResponse(call: Call<List<Post>>, response: Response<List<Post>>) {
                    val posts = response?.body()
                    Log.d("RESP POST", Gson().toJson(posts))
                    posts?.forEach {
                        Log.d("RESP POST", it.body)
                    }
                }
            })
        }


    }
}
