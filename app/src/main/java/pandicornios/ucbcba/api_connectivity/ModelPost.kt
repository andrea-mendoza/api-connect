package pandicornios.ucbcba.api_connectivity

data class ResponseLibrary (val code: Int, val message: String, var result: Post)


data class Post(val userId: Int, val id: Int, val title: String, val body: String)
